from html.parser import HTMLParser


class Parser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.match = False
        self.title = ''
        self.og_title= ''
        self.og_image=''

    def handle_starttag(self, tag, attributes):
        if tag == 'title' :
            self.match = True
        if tag == 'meta':
            for indice in range (0,len(attributes)):
                    if attributes[indice][1]=='og:title':
                        self.og_title = attributes[-1][1]
                    if attributes[indice][1] == 'og:image':
                        self.og_image = attributes[-1][1]


    def handle_data(self, data):
        if self.match:
            self.title = data
            self.match = False

