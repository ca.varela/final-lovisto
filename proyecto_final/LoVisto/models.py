from django.db import models
from django.contrib.auth.models import User


class Modo (models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    oscuro=models.BooleanField(default=False)

class Aportacion (models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    url = models.CharField(max_length=200)
    titulo = models.CharField(max_length=64)
    descripcion = models.TextField()
    num_comen = models.IntegerField(default=0)
    fecha = models.DateTimeField()
    num_positivos = models.IntegerField(default=0)
    num_negativos = models.IntegerField(default=0)

    def __str__(self):
         return str(self.id) + str(self.usuario) + ": " + self.url + " " + self.titulo

class Voto (models.Model):
    usu = models.ForeignKey(User, on_delete=models.CASCADE)
    apor = models.ForeignKey(Aportacion, on_delete=models.CASCADE)
    ELECCIONES=(
        ('like','like'),
        ('dislike', 'dislike'),
        ('normal', 'normal')

    )
    choice= models.CharField(choices=ELECCIONES, max_length=10)
    def __str__(self):
        return str(self.id)

class Aemet (models.Model):
    aportacion= models.OneToOneField (Aportacion, on_delete=models.CASCADE, null= True)
    municipio= models.CharField(max_length=20)
    provincia= models.CharField(max_length= 20)
    copyright= models.CharField(max_length= 50)
    identi= models.CharField(max_length=10)

class Aemet_datos (models.Model):
    aemet= models.ForeignKey(Aemet, on_delete=models.CASCADE)
    fecha= models.TextField()
    temp_max=models.CharField(max_length=2)
    temp_min=models.CharField(max_length=2)
    term_max=models.CharField(max_length=2)
    term_min=models.CharField(max_length=2)
    hum_max=models.CharField(max_length=2)
    hum_min=models.CharField(max_length=2)


class Wikipedia (models.Model):
    aportacion= models.OneToOneField (Aportacion, on_delete=models.CASCADE, null= True)
    articulo = models.CharField(max_length=20)
    texto= models.TextField()
    url_imag= models.CharField(max_length=50)

class Youtube (models.Model):
    aportacion = models.OneToOneField(Aportacion, on_delete=models.CASCADE, null=True)
    video = models.CharField(max_length=30)
    titulo = models.CharField(max_length=30)
    autor= models.CharField(max_length=20)
    iframe= models.CharField(max_length=80)


class No_reconocido (models.Model):
    aportacion = models.OneToOneField(Aportacion, on_delete=models.CASCADE, null=True)
    og_titulo=models.CharField(max_length=100)
    og_imagen=models.CharField(max_length=70)
    titulo=models.CharField(max_length=100)


class Comentario (models.Model): #cuando quiera poner el usuario que ha hecho el comentario creo que puedo hacerlo relacionando
                                #el comentario con su aportacion y su aportacion con su usuario, sino tendre que añadir el usuario aqui
        aportacion = models.ForeignKey(Aportacion, on_delete=models.CASCADE)
        texto = models.TextField()
        url = models.TextField()
        fecha = models.DateTimeField("publicado")
        usuario = models.ForeignKey(User, on_delete=models.CASCADE)







