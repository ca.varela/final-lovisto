from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, get_object_or_404
from .models import Aportacion, Aemet, Aemet_datos, Comentario, Wikipedia, Youtube, No_reconocido, Voto, Modo
from django.contrib.auth.models import User
from .forms import AportacionForm, ComentarioForm , Aportacion_Corta_Form
from urllib.request import urlopen
from urllib.error import HTTPError
from django.utils import timezone
import urllib
from .aemet import Pag_Aemet
from .wiki import Texto_Wiki
from .no_reconocido import Parser
import json
import urllib.request
from django.db import models

class ArticuloInvalido(Exception):
        pass
class IdInvalido(Exception):
        pass
class VideoInvalido(Exception):
        pass
class Pagina_No_Encontrada(Exception):
        pass


def parse_aemet (url):
        try:
                xmlAemet = urllib.request.urlopen(url)
        except HTTPError:
                raise IdInvalido
        datos = Pag_Aemet(xmlAemet)
        aux = datos.aemet()
        return aux

def parse_wiki_texto (url):
        xml_texto_wiki = urllib.request.urlopen(url)
        texto = Texto_Wiki(xml_texto_wiki)
        aux = texto.wiki_texto()
        aux= aux[:400]
        return aux

def parse_wiki_img(url):

        request = urllib.request.Request(url)
        with urllib.request.urlopen(request) as response:
                data = json.loads(response.read().decode('utf8'))
                pages= data['query']['pages']
                id= list (pages.keys())
                page_id= pages[str(id[0])]
                try:
                        source= page_id['thumbnail']['source']
                except KeyError:
                        raise ArticuloInvalido
                return source


def  parse_youtube (url):
        request = urllib.request.Request(url)
        try:
                data = json.loads(urllib.request.urlopen(request).read().decode('utf8'))
        except HTTPError:
                raise VideoInvalido
        titulo= data['title']
        autor= data['author_name']
        html= data ['html']
        aux_iframe= html.split('src=\u0022')[1]
        iframe= aux_iframe.split('?')[0]
        datos_youtube= [titulo,autor, iframe]
        return datos_youtube


def proceso_aemet (recurso, aportacion):

        aux_2 = recurso.split('/')[-1]
        id = aux_2.split('-id')[1]
        url_xml = 'https://www.aemet.es/xml/municipios/localidad_' + id + '.xml'
        parseado = parse_aemet(url=url_xml)
        copyr = parseado.split('+')[0]
        municipio = parseado.split('+')[1].lower()
        provincia = parseado.split('+')[2]
        aemet = Aemet(municipio=municipio, provincia=provincia, identi= id, copyright=copyr)
        aemet.save()
        aux_elementos = parseado.split('+')[3:]
        while aux_elementos:
                aemet_datos = Aemet_datos(aemet=aemet)
                aemet_datos.fecha = aux_elementos.pop(0)
                aemet_datos.temp_max = aux_elementos.pop(0)
                aemet_datos.temp_min = aux_elementos.pop(0)
                aemet_datos.term_max = aux_elementos.pop(0)
                aemet_datos.term_min = aux_elementos.pop(0)
                aemet_datos.hum_max = aux_elementos.pop(0)
                aemet_datos.hum_min = aux_elementos.pop(0)
                aemet_datos.save()

        aemet.aportacion = aportacion
        aportacion.save()
        aemet.save()



def proceso_wikipedia(articulo,aportacion):

        url_xml_text = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=" + articulo + "&prop=extracts&exintro&explaintext"
        url_json_imag = "https://es.wikipedia.org/w/api.php?action=query&titles=" + articulo + "&prop=pageimages&format=json&pithumbsize=100"
        texto_wik = parse_wiki_texto(url=url_xml_text)
        url_imag_wik = parse_wiki_img(url=url_json_imag)
        aportacion.save()
        wik = Wikipedia(aportacion=aportacion, articulo=articulo, texto=texto_wik, url_imag=url_imag_wik)
        wik.save()

def proceso_youtube(video, aportacion):

        url_json = "https://www.youtube.com/oembed?format=json&url=https://www.youtube.com/watch?v=" + video
        info_video = parse_youtube(url=url_json)
        aportacion.save()
        you = Youtube(aportacion=aportacion, video=video, titulo=info_video[0], autor=info_video[1],
                      iframe=info_video[2])
        you.save()

def proceso_no_reconocido(url,aportacion):

        try:
                html_string = str(urlopen(url).read().decode('utf-8'))
        except HTTPError:
                raise Pagina_No_Encontrada
        parser = Parser()
        parser.feed(html_string)
        aportacion.save()
        if len(parser.og_title) != 0 or len(parser.og_image) != 0:
                no_reconocido = No_reconocido(aportacion=aportacion, og_titulo=parser.og_title,
                                              og_imagen=parser.og_image)
        elif len(parser.title) != 0:
                no_reconocido = No_reconocido(aportacion=aportacion, titulo=parser.title)
        else:
                no_reconocido = No_reconocido(aportacion=aportacion)
        no_reconocido.save()


def Voto_Positivo(aportacion,username):

        votos_aportacion= Voto.objects.get(apor=aportacion,usu=username)
        if votos_aportacion.choice == 'normal':
                aportacion.num_positivos = aportacion.num_positivos + 1
                aportacion.save()
                votos_aportacion.choice='like'
                votos_aportacion.save()
        elif votos_aportacion.choice!='like':
                aportacion.num_negativos= aportacion.num_negativos - 1
                aportacion.num_positivos= aportacion.num_positivos + 1
                aportacion.save()
                votos_aportacion.choice='like'
                votos_aportacion.save()




def Voto_Negativo(aportacion,username):
        votos_aportacion= Voto.objects.get(apor=aportacion,usu=username)
        if votos_aportacion.choice == 'normal':
                aportacion.num_negativos = aportacion.num_negativos + 1
                aportacion.save()
                votos_aportacion.choice = 'dislike'
                votos_aportacion.save()
        elif votos_aportacion.choice != 'dislike':
                aportacion.num_negativos = aportacion.num_negativos + 1
                aportacion.num_positivos = aportacion.num_positivos - 1
                aportacion.save()
                votos_aportacion.choice = 'dislike'
                votos_aportacion.save()


def Comprobar_Voto(aportacion, username):

        votos_usuario= Voto.objects.filter(usu=username,apor= aportacion)
        if not votos_usuario:
                voto_normal= Voto(apor=aportacion, usu=username, choice='normal')
                voto_normal.save()


def Votos_last_10 (aportaciones,username):
        for aportacion in aportaciones:
                Comprobar_Voto(aportacion, username)



def proceso_login(request):
        identi = request.POST['identificador']
        contraseña= request.POST['contraseña']
        u= authenticate(request, username=identi, password=contraseña)
        if u is not None:  # estoy autenticado
                login(request, u)

def subir_aportacion(request):
        url = request.POST['url']
        titulo = request.POST['titulo']
        fecha = timezone.now()
        descripcion = request.POST['descripcion']
        aportacion = Aportacion(usuario=request.user, url=url, titulo=titulo,
                                descripcion=descripcion,
                                num_comen=0, fecha=fecha)
        if url.split('//')[0] == "http:" or url.split('//')[0] == "https:":
                aux = url.split('//')[1]
                sitio = aux.split('/')[0]
                if sitio == "www.aemet.es" or sitio == "aemet.es":
                        recurso = aux.split('aemet.es')[1]
                        if recurso[:35] == "/es/eltiempo/prediccion/municipios/":
                                proceso_aemet(recurso, aportacion)

                        else:
                                respuesta = "esta mal el recurso Aemet"
                                return respuesta

                elif sitio == "es.wikipedia.org":
                        recurso = aux.split('es.wikipedia.org')[1]
                        if recurso[:6] == "/wiki/":
                                articulo = recurso.split("wiki/")[1]
                                proceso_wikipedia(articulo, aportacion)
                        else:
                                respuesta = "esta mal el recurso Wikipedia"
                                return respuesta
                elif sitio == "www.youtube.com" or sitio == "youtube.com":
                        recurso = aux.split('youtube.com')[1]
                        if recurso[:9] == "/watch?v=":
                                video = recurso.split("watch?v=")[1]
                                proceso_youtube(video, aportacion)
                        else:
                                respuesta = "esta mal el recurso YouTube"
                                return respuesta
                else:
                        proceso_no_reconocido(url, aportacion)
        else:
                respuesta = "falta añadir http:// o https://"
                return respuesta

def procesar_modo(username,cambio):
        try:
                modo= Modo.objects.get (usuario=username)
        except models.ObjectDoesNotExist:
                modo= Modo(usuario=username,oscuro=False)
                modo.save()
                return modo
        if cambio==True:
                modo.oscuro=not (modo.oscuro)
                modo.save()

def Mostrar_pag_prin(request,cambio):

                aportacion_last_10 = Aportacion.objects.all().order_by('fecha').reverse()[:10]
                aportacion_last_3 = Aportacion.objects.all().order_by('fecha').reverse()[:3]
                if request.user.is_authenticated:
                        username = request.user
                        Votos_last_10(aportacion_last_10, username)
                        aportacion_user_last_5 = Aportacion.objects.filter(usuario=request.user).order_by(
                                'fecha').reverse()[:5]
                        votos_us = Voto.objects.filter(apor=aportacion_last_10)
                        form = Aportacion_Corta_Form()
                        procesar_modo(username,cambio)
                        modo= Modo.objects.get (usuario=request.user)
                        context = {'aportacion_last_10': aportacion_last_10, 'form': form,
                                   'aportacion_user_last_5': aportacion_user_last_5,
                                   'votos_us': votos_us, 'aportacion_last_3': aportacion_last_3, 'oscuro':modo.oscuro}
                        return render(request, 'LoVisto/autenticado.html', context)
                else:
                        context = {'aportacion_last_10': aportacion_last_10, 'aportacion_last_3': aportacion_last_3}
                        return render(request, 'LoVisto/autenticado.html', context)

def Mostrar_pag_usuario(request,cambio):

        procesar_modo(request.user, cambio)
        modo = Modo.objects.get(usuario=request.user)
        aportaciones_user = Aportacion.objects.filter(usuario=request.user)
        comentarios_user = Comentario.objects.all().filter(usuario=request.user)
        votos_user = Voto.objects.filter(usu=request.user)
        aportacion_last_3 = Aportacion.objects.all().order_by('fecha').reverse()[:3]
        context = {'aportaciones_user': aportaciones_user, 'comentarios_user': comentarios_user,
                   'votos_user': votos_user, 'aportacion_last_3': aportacion_last_3,'oscuro':modo.oscuro}
        return render(request, 'LoVisto/usuario.html', context)


def Mostrar_pag_todas_aportaciones(request, cambio):

        lista_aportaciones = Aportacion.objects.all().order_by('fecha')
        aportacion_last_3 = Aportacion.objects.all().order_by('fecha').reverse()[:3]
        if request.user.is_authenticated:
                procesar_modo(request.user, cambio)
                modo = Modo.objects.get(usuario=request.user)
                context = {'lista_aportaciones': lista_aportaciones, 'aportacion_last_3': aportacion_last_3,'oscuro':modo.oscuro}
                return render(request, 'LoVisto/aportaciones.html', context)
        else:
                context = {'lista_aportaciones': lista_aportaciones, 'aportacion_last_3': aportacion_last_3}
                return render(request, 'LoVisto/aportaciones.html', context)


def Mostrat_pag_aportacion(request,cambio,aportacion_unic):

        aportacion_last_3 = Aportacion.objects.all().order_by('fecha').reverse()[:3]

        if request.user.is_authenticated:
                Comprobar_Voto(aportacion_unic, request.user)
                voto_apor = Voto.objects.get(apor=aportacion_unic, usu=request.user)
                procesar_modo(request.user, cambio)
                modo = Modo.objects.get(usuario=request.user)
                form = ComentarioForm()
                context = {'aportacion': aportacion_unic, 'form': form, 'voto_apor': voto_apor,
                           'aportacion_last_3': aportacion_last_3,'oscuro':modo.oscuro}
                return render(request, 'LoVisto/aportacion_unica.html', context)
        else:
                context = {'aportacion': aportacion_unic, 'aportacion_last_3':aportacion_last_3}
                return render(request, 'LoVisto/aportacion_unica.html', context)

def Mostrat_pag_informacion(request, cambio):

        lista_aportaciones = Aportacion.objects.all().order_by('fecha')
        aportacion_last_3 = Aportacion.objects.all().order_by('fecha').reverse()[:3]
        if request.user.is_authenticated:
                procesar_modo(request.user, cambio)
                modo = Modo.objects.get(usuario=request.user)
                context = {'lista_aportaciones': lista_aportaciones, 'aportacion_last_3': aportacion_last_3,'oscuro':modo.oscuro}
                return render(request, 'LoVisto/informacion.html', context)
        else:
                context = {'lista_aportaciones': lista_aportaciones, 'aportacion_last_3': aportacion_last_3}
                return render(request, 'LoVisto/informacion.html', context)

def principal (request):

        cambio = False
        if request.method == "POST":
                action = request.POST['action']
                if action=="Login":
                       proceso_login (request)
                elif action == "Logout":
                        logout(request)
                elif action =="Subir":
                        if subir_aportacion(request):
                                respuesta=subir_aportacion(request)
                                return HttpResponse(respuesta)
                elif action =="Like":
                        username = request.user
                        aportacion = Aportacion.objects.get(id=request.POST['id'])
                        Voto_Positivo(aportacion, username)
                elif action =="Dislike":
                        username = request.user
                        aportacion = Aportacion.objects.get(id=request.POST['id'])
                        Voto_Negativo(aportacion, username)
                else:
                        respuesta = "action no es reconocido, action: " + action
                        return HttpResponse(respuesta)
                return Mostrar_pag_prin(request,cambio)

        elif request.method == 'GET':
                format=request.GET.get('format')
                if format == 'xml' :
                        return redirect('/LoVisto/aportaciones?format=xml')
                elif format == 'json':
                        return redirect('/LoVisto/aportaciones?format=json')
                else:
                        modo = request.GET.get('modo')
                        if modo == 'cambiar_a_oscuro' or modo=='cambiar_a_blanco':
                                cambio=True
                                return Mostrar_pag_prin(request,cambio)
                        else:
                                return Mostrar_pag_prin(request,cambio)




def usuario (request,usuario):

        cambio = False
        if request.user.is_authenticated:
                u = User.get_username(request.user)
                if u == usuario:
                        if request.method == "POST":
                                action = request.POST['action']
                                if action == "Logout":
                                        logout(request)
                                        respuesta = "te has deslogeado por lo que no puedes entrar a tu página de usuario, por favor para entrar a la página de usuario debe volver a logarse"
                                        return HttpResponse(respuesta)
                        elif request.method == 'GET':
                                modo = request.GET.get('modo')
                                if modo == 'cambiar_a_oscuro' or modo == 'cambiar_a_blanco':
                                        cambio = True
                                        return Mostrar_pag_usuario(request, cambio)
                                else:
                                        return Mostrar_pag_usuario(request, cambio)

                else:
                         respuesta= "el nombre de usuario no corresponde con el usuario logado actualmente, porfavor no intente entrar a la página de otro usuario"
                         return HttpResponse (respuesta)

        else:
                respuesta = "no esta autenticado"
                return HttpResponse(respuesta)


def aportaciones (request):

        cambio = False
        if request.method == "POST":
                action = request.POST['action']
                if action == "Login":
                        proceso_login(request)
                elif action == "Logout":
                        logout(request)
                return Mostrar_pag_todas_aportaciones(request,cambio)
        elif request.method == 'GET':
                format = request.GET.get('format')
                if format == 'xml':
                        lista_aportaciones = Aportacion.objects.all().order_by('fecha')
                        context = {'lista_aportaciones': lista_aportaciones}
                        return render(request, 'LoVisto/aportaciones.xml', context)
                elif format == 'json':
                        lista_aportaciones = Aportacion.objects.all().order_by('fecha')
                        context = {'lista_aportaciones': lista_aportaciones}
                        return render(request, 'LoVisto/aportaciones.json', context)
                else:
                        modo = request.GET.get('modo')
                        if modo == 'cambiar_a_oscuro' or modo == 'cambiar_a_blanco':
                                cambio = True
                                return Mostrar_pag_todas_aportaciones(request, cambio)
                        else:
                                return Mostrar_pag_todas_aportaciones(request, cambio)

def aportacion (request, id):

        try:
                aportacion_unic = Aportacion.objects.get(id=id)
        except ValueError:
                return redirect('/LoVisto')
        cambio = False
        if request.method == "POST":
                action = request.POST['action']
                if action == "Login":
                        proceso_login(request)
                elif action == "Logout":
                        logout(request)
                elif action == "Añadir":
                        texto= request.POST['texto']
                        url= request.POST['url']
                        fecha = timezone.now()
                        aportacion_unic.num_comen= aportacion_unic.num_comen +1
                        aportacion_unic.save()
                        comentario = Comentario (aportacion= aportacion_unic, texto=texto, url=url,fecha=fecha, usuario= request.user)
                        comentario.save()
                elif action == "Like":
                        username = request.user
                        aportacion = Aportacion.objects.get(id=request.POST['id'])
                        Voto_Positivo(aportacion, username)
                elif action == "Dislike":
                        username = request.user
                        aportacion = Aportacion.objects.get(id=request.POST['id'])
                        Voto_Negativo(aportacion, username)
                return Mostrat_pag_aportacion (request, cambio,aportacion_unic)
        elif request.method == 'GET':
                modo = request.GET.get('modo')
                if modo == 'cambiar_a_oscuro' or modo == 'cambiar_a_blanco':
                        cambio = True
                        return Mostrat_pag_aportacion(request, cambio, aportacion_unic)
                else:
                        return Mostrat_pag_aportacion(request, cambio, aportacion_unic)


def informacion (request):
        cambio = False
        if request.method == "POST":
                action = request.POST['action']
                if action == "Login":
                        proceso_login(request)
                elif action == "Logout":
                        logout(request)
                return Mostrat_pag_informacion(request, cambio)
        elif request.method == 'GET':
                modo = request.GET.get('modo')
                if modo == 'cambiar_a_oscuro' or modo == 'cambiar_a_blanco':
                        cambio = True
                        return Mostrat_pag_informacion(request, cambio)
                else:
                        return Mostrat_pag_informacion(request, cambio)






