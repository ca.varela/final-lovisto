from django.urls import path
from . import views

urlpatterns = [

    path('aportaciones', views.aportaciones),
    path('informacion', views.informacion),
    path ('<str:id>', views.aportacion),
    path ('usuario/<str:usuario>', views.usuario),
    path('', views.principal),
]