from django.contrib import admin
from .models import Aportacion, Aemet, Aemet_datos,Comentario,Wikipedia, Youtube, No_reconocido, Voto,Modo

admin.site.register(Aportacion)
admin.site.register(Aemet_datos)
admin.site.register(Aemet)
admin.site.register(Comentario)
admin.site.register(Wikipedia)
admin.site.register(Youtube)
admin.site.register(No_reconocido)
admin.site.register(Voto)
admin.site.register(Modo)
