from django import forms
from .models import Aportacion, Comentario

class AportacionForm (forms.ModelForm):
    class Meta:
        model = Aportacion
        fields = ('url','titulo','descripcion','num_comen', 'num_positivos', 'num_negativos')

class Aportacion_Corta_Form (forms.ModelForm):
    class Meta:
        model = Aportacion
        fields = ('url','titulo','descripcion')


class ComentarioForm (forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ('texto','url')