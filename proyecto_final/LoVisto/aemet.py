
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string

class Aemet_Handler(ContentHandler):
    """Class to handle events fired by the SAX parser

    Fills in self.videos with title, link and id for videos
    in a YT channel XML document.
    """

    def __init__ (self):

        self.inOrigen = False
        self.inPrediccion= False
        self.inDia = False
        self.inTemperatura = False
        self.inContent = False
        self.inTermica = False
        self.inHumedad = False
        self.content = ""
        self.municipio = ""
        self.provincia = ""
        self.fecha = ""
        self.temp_maxima= ""
        self.temp_minima = ""
        self.termica_maxima = ""
        self.termica_minima = ""
        self.humedad_maxima = ""
        self.humedad_minima = ""
        self.aemet = ""

    def startElement (self, name, attrs):

        if name == 'origen':
            self.inOrigen = True
        elif self.inOrigen:
            if name == 'copyright':
                self.inContent = True
        elif name == 'nombre':
            self.inContent = True
        elif name == 'provincia':
            self.inContent = True

        elif name == 'prediccion':
            self.inPrediccion = True
        elif self.inPrediccion: #aqui entra a prediccion
            if name == 'dia':
                self.fecha = attrs.get('fecha')
                self.inDia= True
            elif self.inDia:
                if name == "temperatura":
                    self.inTemperatura = True
                elif self.inTemperatura:
                    if name == "maxima":
                        self.inContent = True
                    elif name == "minima":
                        self.inContent = True
                elif name == "sens_termica":
                    self.inTermica = True
                elif self.inTermica:
                    if name == "maxima":
                        self.inContent = True
                    elif name == "minima":
                        self.inContent = True
                elif name == "humedad_relativa":
                    self.inHumedad = True
                elif self.inHumedad:
                    if name == "maxima":
                        self.inContent = True
                    elif name == "minima":
                        self.inContent = True




    def endElement (self, name):
        global aemet

        if self.inOrigen:
            if name == 'copyright':
                self.aemet = self.content
                self.content = ""
                self.inContent = False
                self.inOrigen = False
        elif name == 'provincia':
            self.provincia = self.content
            self.content = ""
            self.inContent = False
            self.aemet+= "+" + self.provincia
        elif name == 'nombre':
            self.municipio = self.content
            self.content = ""
            self.inContent = False
            self.aemet += "+" + self.municipio

        elif self.inPrediccion:
            if self.inDia:
                if self.inTemperatura:
                    if name =="maxima":
                        self.temp_maxima = self.content
                        self.content = ""
                        self.inContent = False
                    elif name == "minima":
                        self.temp_minima = self.content
                        self.content = ""
                        self.inContent = False
                        self.inTemperatura = False
                elif self.inTermica:
                    if name == "maxima":
                        self.termica_maxima = self.content
                        self.content = ""
                        self.inContent = False
                    elif name == "minima":
                        self.termica_minima = self.content
                        self.content = ""
                        self.inContent = False
                        self.inTermica = False
                elif self.inHumedad:
                    if name == "maxima":
                        self.humedad_maxima = self.content
                        self.content = ""
                        self.inContent = False
                    elif name == "minima":
                        self.humedad_minima = self.content
                        self.content = ""
                        self.inContent = False
                        self.inHumedad = False
                elif name == "dia":
                    self.aemet += "+" +self.fecha + "+" + self.temp_maxima+ "+" +self.temp_minima  + "+" +self.termica_maxima + "+" + self.termica_minima+ "+" + self.humedad_maxima+ "+" + self.humedad_minima

                    self.inDia = False



    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class Pag_Aemet:


    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = Aemet_Handler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def aemet (self):

        return self.handler.aemet
