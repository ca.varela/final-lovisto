from django.test import TestCase
from django.test import Client

from .models import Aportacion

class GetTest(TestCase):

    def test_pag_principal(self):
        c = Client()
        respuesta_get = c.get('/LoVisto/')
        self.assertEqual(respuesta_get.status_code, 200)
        
        contenido = respuesta_get.content.decode('utf-8')
        self.assertIn('<a class="nav-link" href=/LoVisto/informacion>Información</a>', contenido)

        respuesta_post= c.post('/LoVisto/',{'action':'Login', 'identificador':'admin', 'contraseña':'admin'})
        self.assertEqual(respuesta_post.status_code,200)

    def test_pag_lista_aportaciones(self):
        c = Client()
        response = c.get('/LoVisto/aportaciones')
        self.assertEqual(response.status_code, 200)

        contenido = response.content.decode('utf-8')
        self.assertIn('<a class="nav-link" href=/LoVisto?format=json>Descarga como fichero JSON</a>', contenido)

        respuesta_post = c.post('/LoVisto/aportaciones', {'action': 'Login', 'identificador': 'admin', 'contraseña': 'admin'})
        self.assertEqual(respuesta_post.status_code, 200)

    def test_pag_aportacion(self):

        todas_aportaciones= Aportacion.objects.all()
        for aportacion in todas_aportaciones:
            c = Client()
            response = c.get('/LoVisto/' + aportacion.id)
            self.assertEqual(response.status_code, 200)

            respuesta_post = c.post('/LoVisto/' + aportacion.id, {'action': 'Login', 'identificador': 'admin', 'contraseña': 'admin'})
            self.assertEqual(respuesta_post.status_code, 200)

    def test_pag_informacion(self):

        c = Client()
        response = c.get('/LoVisto/informacion')
        self.assertEqual(response.status_code, 200)

        contenido = response.content.decode('utf-8')
        self.assertIn('<footer id="pie">', contenido)

        respuesta_post = c.post('/LoVisto/aportaciones',{'action': 'Login', 'identificador': 'admin', 'contraseña': 'admin'})
        self.assertEqual(respuesta_post.status_code, 200)
