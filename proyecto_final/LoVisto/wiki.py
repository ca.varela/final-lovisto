
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string

class Texto_Wiki_Handler(ContentHandler):


    def __init__ (self):

        self.inPages =False
        self.inPage = False
        self.inContent = False
        self.content = ""
        self.wiki_texto = ""

    def startElement (self, name, attrs):
        if name == 'pages':
            self.inPages = True
        elif self.inPages:
            if name == 'page':
                self.inPage =True
            elif self.inPage:
                if name == 'extract':
                    self.inContent= True


    def endElement (self, name):
        global wiki_texto

        if self.inPages:
            if self.inPage:
                if name == 'extract':
                    self.wiki_texto = self.content
                    self.content=""
                    self.inContent = False



    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class Texto_Wiki:


    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = Texto_Wiki_Handler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def wiki_texto (self):

        return self.handler.wiki_texto
