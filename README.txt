# Entrega practica
## Datos

* Nombre:Carlos Arturo Varela Rodríguez
* Titulacion: Sistemas de Telecomunicaciones + ADE
* Despliegue (url): cavarela
* Video basico (url): https://www.youtube.com/watch?v=e_lhmoia2kI
* Video parte opcional (url): https://www.youtube.com/watch?v=fMiWtum13Fs&t=66s
* Despliegue (url): cavarela2.pythonanywhere.com

## Cuenta Admin Site

* admin/admin

## Cuentas usuarios

* admin/admin
* Carmena/comida22
* rosalia/comida22

## Resumen parte obligatoria

Desde cualquier página se puede ir a cualquier otra página desde la lista de navegación, excepto la página de usuario ya que así lo indica la práctica. Para la página principal hay que añadir /LoVisto ,para la de información /LoVisto/informacion ,para la de todas las aportaciones /LoVisto/aportaciones ,para la de una aportacion /LoVisto/(id aportacion) y para la página de usuario /LoVisto/usuario/(nombre de usuario)


## Lista partes opcionales

* Nombre parte: jerarquia de plantillas (madre.html)
* Nombre parte: tarjetas para login y logout 
* Nombre parte: botones para login, logout, likes, dislikes, subir aportación y subir comentario
* Nombre parte: tarjeta para el pie de página
* Nombre parte: grupo de listas para mostrar aportaciones tanto en formato extendido como resumido
* Nombre parte: acordeón siempre abierto para la página de información
* Nombre parte: se puede añadir en el comentario una imagen cuando se añade un comentario
* Nombre parte: todos los comentarios tienen una imagen estática alado del texto del comentario que es el logo de la urjc
* Nombre parte: errores espcificos ArticuloInvalido, IdInvalido,VideoInvalido y Pagina_No_Encontrada
* Nombre parte: si entras como un usuario X e intentas entrar a la página de usuario Y, se muestra un comentario especifico.
